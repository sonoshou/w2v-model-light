from gensim.models import word2vec as GensimWord2Vec
model = GensimWord2Vec.Word2Vec.load('w2v_model/wiki.model')

# 類義語検索
results = model.wv.most_similar(positive=['テスト'])
for result in results:
      print(result)
