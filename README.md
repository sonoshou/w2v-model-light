# 概要

- word2vecを使うサンプル。
- 他のプロジェクトから、word2vecのモデルを別プロジェクトにするために利用した。

# モデルの生成

## モデルの生成方法

1. 2018年某日のwikipedia日本語版全データを収集。
2. mecab-ipadic-neologdで形態素に分かち書き。
3. 分かち書きしたテキストファイルからword2vecモデルを生成。

## モデルのオプション

- 100次元の単語ベクトル
- 50回未満しか登場しなかった単語は無視

# サンプルの実行方法

## 1. pythonの実行環境を用意

```
python3 -m venv vendor/bundle
```

## 2. ライブラリのインストール

```
./vendor/bundle/bin/pip3 install -r requirements.txt
```

## 3. sampleの実行

```
./vendor/bundle/bin/python3 sample.py
```
